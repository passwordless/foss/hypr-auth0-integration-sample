#!/usr/bin/env bash
openssl req \
    -newkey rsa:2048 \
    -x509 \
    -nodes \
    -keyout "./dev.key" \
    -new \
    -out "./dev.crt" \
    -reqexts SAN \
    -extensions SAN \
    -config <(cat /System/Library/OpenSSL/openssl.cnf \
        <(printf '
[req]
default_bits = 2048
prompt = no
default_md = sha256
x509_extensions = v3_req
distinguished_name = dn
[dn]
C = US
ST = New York
L = New York
O = HYPR
OU = Field Innovations
emailAddress = info@hypr.com
CN = %s
[v3_req]
subjectAltName = @alt_names
[SAN]
subjectAltName = @alt_names
[alt_names]
DNS.1 = %s
' "$1" "$1")) \
    -sha256 \
    -days 3650
