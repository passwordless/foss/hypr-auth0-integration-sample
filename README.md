# HYPR + Auth0 Integration Sample

[Auth0](https://auth0.com) is a very popular IDaaS (Identity as a Service) platform that is used by many organizations to enable CIAM (Customer Identity and Access Management) use cases for their applications. While Auth0 provides best-in-class capabilities as an identity provider, it provides very basic phishing-resistant (i.e. FIDO) passwordless capabilities.

[HYPR](https://hypr.com) is a full-featured FIDO SaaS platform offering a full spectrum of FIDO UAF and FIDO2 capabilities for passwordless use cases. These features are especially attractive to organizations who are interested in leveraging the latest FIDO capabilities or who may need to go beyond offering simple FIDO2 synced passkeys to their CIAM customers.

Traditionally, customers have integrated HYPR and Auth0 using SSO via OIDC. But this approach has some shortcomings, primarily with running HYPR with the desired domain (origin) against which passkeys would be enrolled. This project demonstrates how to set up a more _native_ integration of HYPR and Auth0, where the authentication requests to the HYPR server are performed directly (through a proxy) from the Auth0 hosted login page.

## Setup

Follow the instructions in each section below to configure and deploy a full instance of the sample.

### Applications

- [Web App](./apps/web/)
- [Passkey Manager](./apps/rp/)

### Infrastructure

- [Auth0 tenant](./infra/auth0/)
- [HYPR tenant](./infra/hypr)

## Use cases

Once you have the sample running, you should be able to attempt the following use cases. For each use case, begin by navigating to the [Web App](./apps/web/) and clicking the "Login" link. When a use case calls for the user to manage their passkeys, this can be done from the Web App by clicking the "Manage Passkeys" link in the `/profile` page.

### Password user

This use case demonstrates the behavior that an existing user with a password would experience when enrolling and managing passkeys.

Demo video:  
[![Design Flow](./media/password-user-demo-video.thumbnail.png)](./media/password-user-demo-video.mp4)

1. Create a new user, signing up with a password
1. Sign in as that user with the password
   - Be prompted to "go passwordless"
   - Enroll a passkey
1. Sign out and sign in with that passkey
1. Go to Passkey Manager and enroll another passkey
1. Sign out and sign in with the password again
1. Go back to Passkey Manager
   - Be challenged to step-up with the passkey
1. Remove all passkeys
   > This is possible because you still have a password
1. Sign out and sign in again and only have password credential option

### Password + MFA user

This use case is similar to the previous, except the user has enrolled in MFA, which should be verified when they go to create their first passkey.

Demo video:  
[![Password + MFA user Demo](./media/password-mfa-user-demo-video.thumbnail.png)](./media/password-mfa-user-demo-video.mp4)

1. Create a new user, signing up with a password
1. Enroll in MFA
   > This can be done by clicking the "Force MFA" link in the the `/profile` page of the Web App
1. Sign in as that user with the password
   - Be prompted to "go passwordless"
   - Be challenged to step-up with MFA
   - Enroll a passkey

### Passkey only user

This use case demonstrates the onboarding of a brand new user that only uses a passkey.

Demo video:  
[![Passkey only user demo video](./media/passkey-only-user-demo-video.thumbnail.png)](./media/passkey-only-user-demo-video.mp4)

1. Create a new user, signing up with a passkey
1. Sign out and sign in as that user with the passkey
1. Go to Passkey Manager and enroll another passkey
1. Remove a previous passkey
   > Notice you must have at least one passkey since there's no other credential (no password)

## Behavior options

### Classic vs. New Universal Login

You can control the Auth0 login experience, [Classic](https://auth0.com/docs/authenticate/login/auth0-universal-login/classic-experience) or [New](https://auth0.com/docs/authenticate/login/auth0-universal-login/new-experience) Universal Login, using the `USE_CLASSIC_UL` config value in the [Auth0 tenant](./infra/auth0/). See [Configuration](./CONFIG.md) for more information.

Demo video:  
[![Classic vs. New Universal Login demo video](./media/classic-vs-new-ul-demo-video.thumbnail.png)](./media/classic-vs-new-ul-demo-video.mp4)

### FIDO2 and UAF passkeys

HYPR supports both [FIDO2 passkeys](https://www.hypr.com/security-encyclopedia/fido2-web-authentication) as well as [UAF FIDO credentials](https://www.hypr.com/security-encyclopedia/fido-uaf). Both FIDO2 and UAF credentials are now considered "passkeys" by the FIDO Alliance. One is not necessarily better than the other as they both have pros and cons.

Attestation (registration) and assertion (authentication) with FIDO2 passkeys occurs more directly with the platform supporting the application that needs an authenticated user. It now has broad built-in support with most modern web browsers and mobile operating systems, which makes FIDO2 easy to implement and very accessible to end users. However, WebAuthn, the API used by developers within their apps to interact with FIDO2, doesn't offer a lot of control over the UX or the type of passkey that a user selects. In contrast, UAF passkeys leverage a separate (often custom) mobile application as the _only_ authenticator, where all attestation and assertion occur. This means you can completely customize the UX around registration, authentication, and management of the passkey. UAF can also be more secure than FIDO2 since the passkey is bound to the app itself and will never sync.

The biggest drawback to UAF is that implementation requires the developer to build these capabilities into a mobile app. However, the good news is developers can easily accomplish this using the HYPR Mobile SDK ([iOS](https://docs.hypr.com/hyprcloud/docs/sdk-ios-getting-started) and [Android](https://docs.hypr.com/hyprcloud/docs/sdk-android-getting-started)). The [HYPR mobile app](https://www.hypr.com/platform/app) is an example of such an app, which can be used with this Sample.

You can enable support for UAF in the Sample using the `UAF_ENABLED` config value in the [Passkey Manager](./apps/rp/) app.

Demo video:  
[![FIDO2 and UAF passkeys demo video](./media/uaf-demo-video.thumbnail.png)](./media/uaf-demo-video.mp4)

### Enforce step-up with stronger FIDO passkeys

The Sample automatically requires step-up authentication when the user attempts to access the [Passkey Manager](./apps/rp/) app when they're signed in with weaker credentials. For example, if the user has registered a passkey, but has only signed in with a password.

You can go a step further and enforce step-up authentication between passkeys that carry different AALs ([Authentication Assurance Levels](https://pages.nist.gov/800-63-3-Implementation-Resources/63B/AAL/)). For example, if a user has registered an AAL3 passkey, like a device-bound passkey (eg. Yubikey) or a UAF passkey but they're currently signed in with a synced FIDO2 passkey (eg. Apple iCloud Keychain), which is only AAL2. This enforcement can be enabled with the `ENFORCE_AAL_STEP_UP` config value. The step-up prompt will also inform the user which passkeys they need to authenticate with to satisfy the assurance level requirements.

Demo video:  
[![Enforce step-up with stronger FIDO passkeys demo video](./media/passkey-step-up-demo-video.thumbnail.png)](./media/passkey-step-up-demo-video.mp4)

## Design

The following diagram is a good representation of the core design of the sample, showing an authentication (assertion) flow between the main Web App, Auth0 service, the Passkey Manager, and the HYPR service. See the [Design](./DESIGN.md) page for more details on this and other flows which are designed to address the above use cases.

![Design Flow](./media/existing-passkey.png)

## Contributing

Any of the items listed in the [Knows Issues](./DESIGN.md#known-issues) section of the design doc are fair game!
