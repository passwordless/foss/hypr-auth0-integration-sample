# Sample Design

## Key Concepts

Keep the following key design concepts in mind when examining the design [flows](#flows).

### Reverse proxy

The [Passkey Manager](./apps/rp/) web application actually has two important roles. First, it's the UX that a user interacts with to manage their passkeys (enroll, view, delete); hence the name "Passkey Manager". But it also serves as a "reverse proxy" in front of the HYPR FIDO server. This reverse proxy behavior is necessary because HYPR's FIDO endpoints cannot be called _directly_ from a public client such as a web browser. Rather, they must be called from a confidential client as they require a server-side access token to accompany any calls.

Therefore, the Passkey Manager exposes the same FIDO endpoints and forwards requests to those endpoints to the HYPR server. Additionally, any endpoints that result in a successful authentication or registration event (eg. `/assertion/result`) also return a [JWT passkey security token](#jwt-passkey-security-token).

### FIDO Relying Party

In a standard FIDO configuration there are two key roles: the FIDO server and the RP (relying party) server. You would therefore expect the [Web App](./apps/web/) in this sample to play the role of RP. However, it's the Auth0 login page (which is hosted by the Auth0 tenant) that needs to be the RP since it hosts the web origin (domain) from which the FIDO requests are being made. That being said, there are also times when passkeys are being generated within the [Passkey Manager](./apps/rp/) app (eg. when a user is adding another passkey), so one could argue that this is the RP app. The reality is both the Auth0 login page _and_ the Passkey Manager need to play this role. Therefore, both need to be configured with custom domains that share a parent domain.

### JWT passkey security token

In a standard FIDO integration, the RP can convey a successful FIDO authentication to the web browser by generating an HTTP cookie, representing the user's session. However, in this sample the actual FIDO authentication calls are occurring against the [Passkey Manager](./apps/rp/) app which is then [forwarding them](#reverse-proxy) to the HYPR server. Therefore, there needs to be a cross-origin mechanism to securely transmit a passkey authentication or registration event from the Passkey Manager to the Auth0 login page. Hence, the use of a JWT, which is signed by the Passkey Manager backend and verified by the Auth0 backend.

### Auth0 custom database connection

Auth0 needs a way to somehow validate a user who has authenticated with HYPR. While it doesn't contain an "authenticator" capability (yet), Auth0 does provide an extensible way to authenticate a password-based user called a [custom database connection](https://auth0.com/docs/authenticate/database-connections/custom-db). A custom database exposes two important hooks: the [`login` action script](https://auth0.com/docs/authenticate/database-connections/custom-db/templates/login) and the [`create` action script](https://auth0.com/docs/authenticate/database-connections/custom-db/templates/create), which can be used to authenticate or create a user in an external system. This sample leverages these endpoints to use the [JWT passkey security token](#jwt-passkey-security-token) as the "password", validate it, and then uses is its claims to create or update an Auth0 user profile.

Two important side-effects should be noted here:

- The JWT itself never gets _stored_ within Auth0, not even as a password credential, since with custom databases, the password credential is considered to be external to Auth0
- Regardless of how many passkeys a user has enrolled in HYPR, they will only ever have a single "passkey" identity in the Auth0 tenant. Specific passkey information is updated in the Auth0 user profile whenever the user authenticates, similar to what happens with a federated connection in Auth0.

### Account linking

To complete the design started with the [custom database connection](#auth0-custom-database-connection), the sample leverages Auth0 [account linking](https://auth0.com/docs/manage-users/user-accounts/user-account-linking) to link a user's password identity with their passkeys identity. Linking these identities together makes it easy in the future to check if a user already has a passkey. If a user creates an Auth0 account with passkey only, then no account linking is necessary.

### Switching connections for New Universal Login

This sample supports password and passkey authentication for both the [Classic Universal Login](https://auth0.com/docs/authenticate/login/auth0-universal-login/classic-experience) and [New Universal Login](https://auth0.com/docs/authenticate/login/auth0-universal-login/new-experience) experiences in Auth0. While having plenty of benefits, New Universal Login lacks some of the connection-detection flexibility that Classic has.

This sample relies on the fact that there are _two_ active database connections during authentication, one to authenticate a user with their password and one for passkeys. Classic UL can handle this using the [Auth0.js SDK](https://auth0.com/docs/libraries/auth0js) to perform a database authentication ([`webauth.login()` method](https://auth0.com/docs/libraries/auth0js#webauth-login-)) against the desired database. New Universal Login can't do this. However, we can control the active connection via the `connection` OIDC parameter. When a switch from one connection to another is needed (eg. the user wishes to use their password instead of a passkey), the New Universal Login [page template](https://auth0.com/docs/customize/universal-login-pages/universal-login-page-templates) can leverage the `/switch` endpoint which is served up by both the [Web App](./apps/web/) and [Passkey Manager](./apps/rp/) app.

### Passkey autofill security tokens for New Universal Login

If the `PASSKEY_AUTOFILL_ENABLED` [config value](./CONFIG.md) is set to `true`, the sample will attempt passkey autofill (aka [WebAuthn Conditional UI](https://passkeys.dev/docs/use-cases/bootstrapping/)). If `USE_CLASSIC_UL` is set to `false` (so the [New Universal Login](https://auth0.com/docs/authenticate/login/auth0-universal-login/new-experience) experience is being rendered by Auth0), something additional is required to transmit the [JWT security token](#jwt-passkey-security-token) that results from the autofill in the initial login prompt (`login-id`) to the "password" (`login-password`) prompt. The sample accomplishes this by temporarily storing the token in the session of the Passkey Manager app via the `/token/store` and `/token/recall` endpoints.

## Flows

The following flows demonstrate how the sample works in addressing the various use cases.

### Authentication with a password (legacy)

This flow is included as a baseline. Most CIAM systems will need to begin the passwordless journey with their preexisting users continuing to authenticate with a password, so we will start there. In this sample, we will also layer on opt-in MFA, so at least the assurance level is closer to that of passkeys.

![Design Flow](./media/legacy-password.png)

1. The user performs an action in the [Web App](./apps/web/) to force authentication (eg. clicks Login button)
1. The Web App initiates an OIDC flow with Auth0 (redirects the browser to the `/authorize` endpoint)
1. No session exists with Auth0, so it redirects to the login page
1. The user is presented with an ["identifier-first" login page](https://auth0.com/docs/authenticate/login/auth0-universal-login/identifier-first) where they are prompted to type a username to continue
1. The user submits their password
1. The credentials are verified against the configured Auth0 database connection
1. If needed (eg. step-up authentication request) Auth0 redirects to the MFA page where the user is challenged to provide an OTP with a previously enrolled authenticator app
1. The user enters a valid OTP
1. The MFA page submits the OTP back to Auth0, which completes the authentication
1. Finally, Auth0 redirects the browser back to the Web App with the usual issued tokens (ID Token and Access Token) to complete the OIDC flow

### Enrolling a new passkey with an existing account

This flow shows how one of these existing password-based users might be encouraged to "go passwordless" and create their first passkey. After they do so, they will have the option to either sign in with their old password or with their new passkey.

![Design Flow](./media/migration-new-passkey.png)

1. This flow begins almost where the [Authentication with a password](#authentication-with-a-password-legacy) flow ends with a user that has successfully authenticated with a password and MFA
1. But instead of redirecting back to the app with OIDC tokens, a [Login](https://auth0.com/docs/customize/actions/flows-and-triggers/login-flow) Auth0 Action is run
1. The `onExecutePostLogin` event handler of the action checks to see if the current user already has a registered passkey. This is done by examining the user profile for the presence of an already [linked passkey identity](#account-linking). If it does exists, then nothing further needs to happen, and the flow jumps to the last step, directing to the app with Auth0 tokens.
1. However, if a passkey identity does not exist, then an [action redirect](https://auth0.com/docs/customize/actions/flows-and-triggers/login-flow/redirect-with-actions) is performed, redirecting the browser to the [Passkey Manager](./apps/rp/) app.
1. The Passkey Manager is a separate web app that must obtain its own session with Auth0, so an OIDC redirect flow occurs, issuing its own ID Token, which is then used by the Passkey Manager to create a local session.
1. The Passkey Manager app then prompts the user if they want to create a passkey. If they decline, then the Passkey Manager redirects the browser back to Auth0 via the `/continue` endpoint.
   > If the user has enrolled in MFA with Auth0, they may be prompted to "step up" with MFA before being able to enroll a passkey
1. If the user confirms then the username is extracted from the Auth0 ID Token and the WebAuthn attestation (registration) flow begins, prompting the user to register a new passkey. This involves two calls to the server. The first is the `options` call, which returns a FIDO challenge from the server.
1. Second is the `result` call, which sends the signed challenge back to the server for verification along with the corresponding public key
1. During the previous step, the HYPR FIDO Server registers the new user and the associated passkey information (eg. public key)
1. The Password Manager then generates a [JWT passkey security token](#jwt-passkey-security-token) that represents the successful registration and then uses the Auth0 Management API to create a new user within a [custom database, specifically used to represent the passkeys of a user](#auth0-custom-database-connection), passing the JWT as the "password"
1. This invokes the custom database connection's `create` script, which simply verifies the JWT
1. Once the new passkey user is created in Auth0, it is then [linked to the password-based user identity in Auth0](#account-linking). Now Auth0 and HYPR are in sync.
1. Then Passkey Manager redirects the browser back to Auth0 (to the `/continue` endpoint) to resume the authentication flow there. This triggers the `onContinuePostLogin` handler of the same Auth0 action that ran before is invoked, which completes the redirect portion of the flow.
1. Finally, Auth0 redirects the browser back to the Web App with the usual issued tokens (ID Token and Access Token) to complete the OIDC flow

### Authentication with existing passkey

Now some users have registered passkeys. This flow shows how one of those users could be prompted to authenticate with their passkey instead of their password:

![Design Flow](./media/existing-passkey.png)

1. The user performs an action in the [Web App](./apps/web/) to force authentication (eg. clicks Login button)
1. The Web App initiates an OIDC flow with Auth0 (the browser is redirected to the `/authorize` endpoint)
1. No session exists with Auth0, so the browser is redirected to the login page
1. The user is presented with an ["identifier-first" login page](https://auth0.com/docs/authenticate/login/auth0-universal-login/identifier-first) where they are prompted to type a username to continue.  
   However, there are a couple possibilities that could happen here:

   - If the user has already registered one or more passkeys, they will automatically be prompted to use them before having to enter a password
   - If no such passkey exists (or if they cancel the discovered ones), they must continue on to enter a password

1. At this point, the first FIDO assertion call (`options`) is made to the server. This is actually done by making an HTTP POST call (via the `fetch` API) to the [Passkey Manager](./apps/rp/), which in turn [proxies the call to the HYPR FIDO server](#reverse-proxy)

1. The `options` call returns a FIDO challenge that the browser must sign using the private key of the passkey. This is when the user actually gets prompted to authenticate (eg. with a biometric), which "unlocks" the ability to use the private key for signing. The signed challenge is then passed back in a second FIDO assertion call (`result`) to the server. Authentication is a successful if the server can validate that the challenge was signed correctly via the corresponding public key.
1. During the previous two steps, the HYPR FIDO Server verifies the calls using an existing user record containing all of the passkeys that have previously been registered
1. Assuming the user authentication is successful with the HYPR FIDO Server, the Passkey Manager server responds to the `result` call from the browser with a [JWT passkey security token](#jwt-passkey-security-token) that contains all of the information needed by Auth0 to identify and authenticate the user (specifically the HYPR `user_id` and `username`)
1. The login page then uses the JWT returned by the FIDO authentication process to submit as the password
1. This invokes the `login` action script of the associated custom database connection, which verifies the JWT and returns a user profile to Auth0, which completes the authentication
1. Authentication in the `login` script either creates or updates a new user record within the Auth0 user directory
1. Finally, Auth0 redirects the browser back to the Web App with the usual issued tokens (ID Token and Access Token) to complete the OIDC flow

### New account registration with passkey only

Ideally there will be first-time users who will wish to create their new account _only_ with a passkey (no password, please). This flow is very similar to the [authenticating with an existing passkey](#authentication-with-existing-passkey) in that the passkey registration occurs on the hosted Auth0 login page instead of the Passkey Manager.

![Design Flow](./media/new-only-passkey.png)

The steps in this flow are nearly identical to the [authentication with an existing passkey](#authentication-with-existing-passkey) flow, except:

- Step 4: The user experience for registering a new user with a passkey is quite simple compared to the authentication flow, since the user is either choosing to create a passkey or (if they're not ready yet), create their account with a password
- Steps 5, 7, 8: The FIDO flow is performing attestation (registering a new passkey) instead of assertion (authenticating an existing passkey)
- Step 6: There's an important spot in the attestation flow where the proxy server needs to make sure a user with the same username (`email`) hasn't already been registered in either Auth0 or HYPR. This is done to prevent account takeover.

### Managing passkeys

Now that a user has enrolled their first passkey (either with a new account or with their existing password-based account), at some point they will want to maybe enroll additional passkeys. Or maybe they have multiple passkeys enrolled and they wish to remove one.

![Design Flow](./media/manage-passkeys.png)

1. Let's assume the user is already signed into the [Web App](./apps/web/). To manage their passkeys, they would click a button or link that takes them directly to the [Passkey Manager](./apps/rp/).
1. Assuming the Passkey Manager app doesn't already have a session, it would perform an OIDC flow with Auth0.
1. Authentication occurs with Auth0. The user probably already has a session, so this would result in SSO.
1. The Passkey Manager gets an ID Token from Auth0 which can be used to create a local session that identifies the user who's passkeys are being managed
1. We cannot allow a user to manage their passkeys if they didn't use a passkey to authenticate in the first place. This can be enforced using custom claims present on the ID Token, signaling whether or not the Passkey Manager needs to first perform a "step-up" to a passkey authentication.
1. The user performs passkey management activities (eg. add new passkey, remove existing passkey)
1. Passkeys changes are reflected in the HYPR user database
1. The Passkey Manager then syncs those changes (if needed) to Auth0. For example, this could be the first passkey that a user has enrolled, in which case they will not have a passkey identity in Auth0 yet. This step ensures that, and it also performs the necessary [account linking](#account-linking) with the password identity (if it exists).

### Authentication with UAF passkeys

One of the passkey types that a user can enroll in the Passkey Manager is a FIDO UAF credential. The overall registration and authentication flows are very similar to FIDO2, except that the actual assertion process happens exclusively between the FIDO server and a mobile application. The job of the RP is to kick off the authentication session and then poll for results, waiting for the authentication ceremony to complete on the mobile app. From the perspective of the RP, the authentication flow is more OOB (out-of-band) than the FIDO2 flow.

The Sample has been designed to perform a UAF authentication in the Auth0 login experience if:

- the only credential(s) that a user has registered are UAF
- the user cancels FIDO2 authentication prompts and explicitly chooses to authenticate with UAF

This flow shows what the resulting UAF authentication looks like:

![Design Flow](./media/uaf-authentication.png)

1. The user performs an action in the [Web App](./apps/web/) to force authentication (eg. clicks Login button)
1. The Web App initiates an OIDC flow with Auth0 (the browser is redirected to the `/authorize` endpoint)
1. No session exists with Auth0, so the browser is redirected to the login page
1. The user is presented with an ["identifier-first" login page](https://auth0.com/docs/authenticate/login/auth0-universal-login/identifier-first) where they are prompted to type a username to continue.
1. To perform a UAF authentication, the browser makes a call to the UAF assertion `start` endpoint in the Passkey Manager to initiate an authentication flow. This call is forwarded to the HYPR FIDO server.
1. The result from the FIDO server (which is echoed back to the browser by the Passkey Manager) is a QR code image and a session ID.
1. The user opens the respective mobile app (either the HYPR app or an app using the HYPR Mobile SDK) and scans the QR code.
1. Meanwhile the browser uses the session ID to perform polling calls (every few seconds) to the UAF assertion `state` endpoint on the Passkey Manager to check the status of the authentication. Like the `start` endpoint, these calls are forwarded by the Passkey Manager to the HYPR FIDO server.
1. The user completes the authentication ceremony on the mobile app (eg. with a biometric), which also interacts with the HYPR FIDO server, proving possession of the UAF credential using a cryptographically-signed challenge (similar to FIDO2).
1. Now that the user is authenticated, the next call to the UAF assertion `state` endpoint returns success with a [JWT passkey security token](#jwt-passkey-security-token) that contains all of the information needed by Auth0 to identify and authenticate the user (specifically the HYPR `user_id` and `username`)

The remaining steps are identical to the FIDO2 authentication flow described in a [previous section](#authentication-with-existing-passkey).

## Known Issues

### Authentication with HYPR passkey doesn't generate `acr` and `amr` claims in ID Token

Since passkeys are arguably an even stronger form of authentication than traditional MFA, an OIDC flow that involves the user authenticating with a HYPR passkey should result in an ID Token that contains the `acr` and `amr` claims per [RFC-8176](https://datatracker.ietf.org/doc/html/rfc8176). Unfortunately, Auth0 does not allow you to set these claims directly. They are set automatically by the platform only when one of the OOTB MFA providers is used. To make up for this shortcoming, the sample generates custom claims that communicate how the user has authenticated. However, if MFA has occurred, then `acr` and `amr` claims are also returned in the ID Token.

### JWT security token should be signed with an asymmetric key

To prevent the practice of configuring the same shared symmetric key between the Passkey Manager and the Auth0 tenant, an asymmetric (public and private) key pair should be used instead. The private key would be stored in the Passkey Manager. The Auth0 tenant would be given access to the public key, via direct configuration or perhaps a [JWKS](https://datatracker.ietf.org/doc/html/rfc7517) endpoint on the Passkey Manager.

### Token replay risk

During the [existing passkey flow](#authentication-with-existing-passkey), the JWT that is used to authenticate the user in the `login` script is passed front-channel. Therefore, there is the possibility of token replay attack. The risk is minimized with a short-lived token (eg. 1 minute) since it only needs to be valid for the duration of the flow.

### Passkey Manager forms need CSRF protection

Presently the Passkey Manager (RP) app doesn't employ any CSRF protection with its HTML forms. Any functional CSRF-protection library (eg. [csrf-csrf](https://github.com/Psifi-Solutions/csrf-csrf)) would work for this.
