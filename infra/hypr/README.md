# HYPR tenant

The HYPR tenant used by the sample.

## RP Application

Create a new [RP application](https://docs.hypr.com/hyprcloud/docs/cc-adv-application-new) in the HYPR tenant and configured it as follows.

- Decentralized channel: `Web`
- IdP provider: `Custom Solution`
- Push notifications: `No, I don't want to enable push notifications`
- Name: `Auth0 Sample`
- Description: `Demonstrates how to set up a more native integration of HYPR and Auth0`

### Policy Management

1. `defaultRegAction`

   - Default registration action
   - `Native`

2. `defaultAuthAction`

   - Default authentication action
   - `Native`

3. `completeMediumTransaction`
   - Default transaction action
   - `PIN`

### Access Tokens

Create a new access token used by the [Passkey Manager](../../apps/rp) app:

- Name: `Auth0`
- Type: `API Token`
- Permissions: `User Management`, `Device Registration`, `Authentication`, `Reporting`

### Login Settings

- QR Authentication [Web Clients Only]: `true`
- Allow Manual QR Entry: `true`

You must also set the `baseURL` and `rpAppId` values in the RP app using the [Create or update Device Manager configuration](https://apidocs.hypr.com/#6b51b4c2-9fca-4df5-8175-ea7e5bbb2b42) HYPR API endpoint.

From the [configuration](../../CONFIG.md), set:

- `baseURL` = `HYPR_BASE_URL`
- `rpAppId` = `HYPR_RP_APP_ID`

### FIDO2 Settings

- Enable Fido2: `true`
- Client Origin URL: A URL that uses only the parent domain of either the Auth0 custom domain or the Passkey Manager web app. Eg. `https://example.com`
- Discoverable Credentials: `Required`
- User Verification Mode: `Required`
- Attestation Type: `Direct`
