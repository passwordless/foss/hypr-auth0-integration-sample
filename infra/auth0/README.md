# Auth0 tenant

The Auth0 tenant used by the sample.

## Prerequisites

- [Auth0 Deploy CLI](https://github.com/auth0/auth0-deploy-cli)

## Setup

1. Create a tenant in Auth0

1. Configure a [custom domain](https://auth0.com/docs/customize/custom-domains) that shares a parent domain with the [Passkey Manager](../../../apps/rp/) app.  
   Example:

   - Auth0 tenant: `login.`**`example.com`**
   - RP app: `passkeys.`**`example.com`**

1. Create a `config.json` file (per the CLI [configuration instructions](https://github.com/auth0/auth0-deploy-cli#configure-the-deploy-cli)) in this directory that points to your tenant.  
   Make sure:

   - Set the `AUTH0_ALLOW_DELETE` value to `true` if you want to wipe out entities that aren't defined in this configuration; otherwise `false`

1. Perform an initial import of the local configuration into your Auth0 tenant:

   ```bash
   a0deploy import -c config.json -i ./tenant.yaml
   ```

1. Modify the `config.json` file, so that it contains a `AUTH0_KEYWORD_REPLACE_MAPPINGS` section with the following values:

   > See [Configuration](../../../CONFIG.md) for descriptions of each config item.

   - `AUTH0_DOMAIN`
   - `PRIMARY_WEB_APP_NAME`
   - `PRIMARY_WEB_APP_BASE_URL`
   - `RP_APP_NAME`
   - `RP_APP_BASE_URL`
   - `TOKEN_SECRET`
   - `PASSWORD_DB_CONNECTION`
   - `PASSKEY_DB_CONNECTION`
   - `ACTIONS_M2M_CLIENT_ID`
   - `ACTIONS_M2M_CLIENT_SECRET`
   - `HYPR_BASE_URL`
   - `USE_CLASSIC_UL`
   - `PASSKEY_AUTOFILL_ENABLED`
   - `LOGO_URL`
   - `PAGE_BACKGROUND_IMAGE_URL`
   - `PRIMARY_BUTTON_COLOR`
   - `PAGE_BACKGROUND_COLOR`
   - `PRIMARY_LABEL_COLOR`
   - `TENANT_FRIENDLY_NAME`
   - `TENANT_NEW_UL_TITLE`
   - `UAF_APP_NAME`

1. Install the necessary features by importing the local configuration into your Auth0 tenant again:

   ```bash
   a0deploy import -c config.json -i ./tenant.yaml
   ```

## Tenant Configuration Export

If you ever need to export the configuration out of the tenant so you can use it to update the local files, perform this command:

```bash
a0deploy export -c config.json -f yaml -o ./export
```

> This command will place the resulting files in an `./export` subdirectory, which has been excluded from source control so data within can easily be cherry-picked and added to the main files as needed.
