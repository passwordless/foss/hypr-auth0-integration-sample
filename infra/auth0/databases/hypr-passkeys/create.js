function create(user, callback) {
  const jwt = require("jsonwebtoken");

  let payload;
  try {
    payload = jwt.verify(user.password, configuration.TOKEN_SECRET);
  } catch (err) {
    return callback(new Error(`Token validation failed: ${err.message}`));
  }
  if (payload.username !== user.email) {
    return callback(new Error("Token username was incorrect."));
  }

  const profile = {
    user_id: payload.user_id,
    email: payload.username,
    devices: payload.devices,
    this_device: payload.this_device,
  };
  callback(null, profile);
}
