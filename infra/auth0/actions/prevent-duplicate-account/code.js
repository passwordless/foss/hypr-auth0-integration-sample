const { AuthenticationClient, ManagementClient } = require("auth0");

async function getAuth0AccessToken(event, api) {
  const tokenRecord = api.cache.get("auth0_access_token");

  if (tokenRecord) {
    console.log("Token fetched from cache");
    return tokenRecord.value;
  } else {
    const authClient = new AuthenticationClient({
      domain: event.secrets.AUTH0_DOMAIN,
      clientId: event.secrets.ACTIONS_M2M_CLIENT_ID,
      clientSecret: event.secrets.ACTIONS_M2M_CLIENT_SECRET,
    });

    const grantResponse = await authClient.oauth.clientCredentialsGrant({
      audience: `https://${event.secrets.AUTH0_DOMAIN}/api/v2/`,
    });
    const {
      data: { access_token },
    } = grantResponse;
    api.cache.set("auth0_access_token", access_token, { ttl: 900000 });

    console.log("New token obtained and stored in cache");
    return access_token;
  }
}

/**
 * Handler that will be called during the execution of a PreUserRegistration flow.
 *
 * @param {Event} event - Details about the context and user that is attempting to register.
 * @param {PreUserRegistrationAPI} api - Interface whose methods can be used to change the behavior of the signup.
 */
exports.onExecutePreUserRegistration = async (event, api) => {
  if (event.client?.name === event.secrets.RP_APP_NAME) {
    console.log("SKIP: Current application is the Passkey Manager");
    return;
  }

  const token = await getAuth0AccessToken(event, api);
  const management = new ManagementClient({
    domain: event.secrets.AUTH0_DOMAIN,
    token,
  });

  const getUsersResults = await management.users.getAll({
    q: `email:"${event.user.email}"`,
  });
  if (getUsersResults.data.length > 0) {
    // prevent a password user registration with the same username as an existing passkey registration
    // which would result in possible account takeover
    console.log(
      `User registration blocked: User '${event.user.email}' already exists in Auth0.`
    );
    api.access.deny("Invalid sign up", "duplicate_account_exists");
  }
};
