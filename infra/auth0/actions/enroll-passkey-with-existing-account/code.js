/**
 * Handler that will be called during the execution of a PostLogin flow.
 *
 * @param {Event} event - Details about the user and the context in which they are logging in.
 * @param {PostLoginAPI} api - Interface whose methods can be used to change the behavior of the login.
 */
exports.onExecutePostLogin = async (event, api) => {
  if (event.connection.name === event.secrets.PASSKEY_DB_CONNECTION) {
    console.log("SKIP: User authenticated with a passkey");
    return;
  }

  if (event.client.name === event.secrets.RP_APP_NAME) {
    console.log("SKIP: Current application is the Passkey Manager");
    return;
  }

  if (event.stats.logins_count === 1) {
    console.log(
      "SKIP: First time the user is logging in. We'll hit them up next time"
    );
    return;
  }

  if (
    event.transaction?.acr_values?.includes(
      "http://schemas.openid.net/pape/policies/2007/06/multi-factor"
    )
  ) {
    console.log("SKIP: User is performing MFA step-up");
    return;
  }

  if (
    !event.user.identities.some(
      (i) => i.connection === event.secrets.PASSKEY_DB_CONNECTION
    )
  ) {
    console.log(
      "No linked passkey identities. Redirecting to Passkey Manager..."
    );
    api.redirect.sendUserTo(`${event.secrets.RP_APP_BASE_URL}/go_passwordless`);
  }
};

/**
 * Handler that will be invoked when this action is resuming after an external redirect. If your
 * onExecutePostLogin function does not perform a redirect, this function can be safely ignored.
 *
 * @param {Event} event - Details about the user and the context in which they are logging in.
 * @param {PostLoginAPI} api - Interface whose methods can be used to change the behavior of the login.
 */
exports.onContinuePostLogin = async (event, api) => {
  // FUTURE: maybe do something here
};
