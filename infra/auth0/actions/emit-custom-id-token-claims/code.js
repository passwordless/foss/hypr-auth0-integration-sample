/**
 * Handler that will be called during the execution of a PostLogin flow.
 *
 * @param {Event} event - Details about the user and the context in which they are logging in.
 * @param {PostLoginAPI} api - Interface whose methods can be used to change the behavior of the login.
 */
exports.onExecutePostLogin = async (event, api) => {
  const isPasswordConnection =
    event.connection.name === event.secrets.PASSWORD_DB_CONNECTION;
  const isPasskeyConnection =
    event.connection.name === event.secrets.PASSKEY_DB_CONNECTION;

  const passwordIdentity = event.user.identities.find(
    (i) => i.connection === event.secrets.PASSWORD_DB_CONNECTION
  );
  const passkeyProfile =
    isPasskeyConnection && event.user.identities.length === 1
      ? event.user
      : event.user.identities.find(
          (i) => i.connection === event.secrets.PASSKEY_DB_CONNECTION
        )?.profileData;

  const factorsAvailable = [
    ...(passwordIdentity ? ["password"] : []),
    ...(event.user.multifactor?.length ? ["mfa"] : []),
    ...(passkeyProfile
      ? Array.from(
          new Set(passkeyProfile.devices.map((d) => `passkey:aal${d.aal}`))
        )
      : []),
  ];
  api.idToken.setCustomClaim("authn_factors_available", factorsAvailable);

  let firstFactorPerformed = "unknown";
  if (isPasswordConnection) {
    firstFactorPerformed = "password";
  } else if (isPasskeyConnection) {
    const thisDevice = passkeyProfile.devices.find(
      (d) => d.device_id === passkeyProfile.this_device
    );
    firstFactorPerformed = `passkey:aal${thisDevice.aal}`;
  }
  api.idToken.setCustomClaim(
    "authn_first_factor_performed",
    firstFactorPerformed
  );
};

/**
 * Handler that will be invoked when this action is resuming after an external redirect. If your
 * onExecutePostLogin function does not perform a redirect, this function can be safely ignored.
 *
 * @param {Event} event - Details about the user and the context in which they are logging in.
 * @param {PostLoginAPI} api - Interface whose methods can be used to change the behavior of the login.
 */
// exports.onContinuePostLogin = async (event, api) => {
// };
