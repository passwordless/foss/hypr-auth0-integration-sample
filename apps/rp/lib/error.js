class ErrorWithStatusCode extends Error {
  constructor(statusCode, message) {
    super(getReasonPhrase(statusCode) + (message ? `: ${message}` : ""));

    this.statusCode = statusCode;
  }

  statusCode;
}

const BadRequestError = (message) => new ErrorWithStatusCode(400, message);

module.exports = {
  ErrorWithStatusCode,
  BadRequestError,
};
