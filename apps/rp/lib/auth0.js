const { ManagementClient } = require("auth0");

const { createPasskeyJwt } = require("./auth");
const { getAllDevices } = require("./hypr");

const auth0Management = new ManagementClient({
  domain: process.env.AUTH0_DOMAIN,
  clientId: process.env.CLIENT_ID,
  clientSecret: process.env.CLIENT_SECRET,
});

async function getUser(username) {
  const getUsersResults = await auth0Management.users.getAll({
    q: `email:"${username}"`,
  });
  const { data: users } = getUsersResults;

  if (users.length === 0) {
    return undefined;
  }
  if (users.length > 1) {
    throw new Error(
      `Multiple unlinked users exist in IdP with username: ${username}`
    );
  }

  const user = users[0];
  const [primary, ...secondaries] = user.identities;
  const passkeyIdentity = secondaries.find(
    (i) => i.connection === process.env.PASSKEY_DB_CONNECTION
  );

  return {
    user_id: user.user_id,
    email: user.email,
    isPasskey: primary.connection === process.env.PASSKEY_DB_CONNECTION,
    passkeyIdentity,
  };
}

async function addPasskeyIdentity(user) {
  if (user.isPasskey) {
    throw new Error(
      "Cannot add a passkey identity to an existing passkey user"
    );
  }
  if (user.passkeyIdentity) {
    throw new Error("User already has an added passkey identity");
  }

  const { email } = user;
  const { fido2Devices, uafDevices } = await getAllDevices(email);
  const devices = [...fido2Devices, ...uafDevices];
  const password = createPasskeyJwt(email, devices);

  // create user in passkeys connection
  const createResult = await auth0Management.users.create({
    connection: process.env.PASSKEY_DB_CONNECTION,
    email,
    password,
    verify_email: false,
  });
  const { data: newUser } = createResult;
  console.log("Auth0 passkey user created:", { user_id: newUser.user_id });
  const passkeyIdentity = newUser.identities[0];

  // link to primary user
  await auth0Management.users.link(
    { id: user.user_id },
    {
      provider: passkeyIdentity.provider,
      connection_id: process.env.PASSKEY_DB_CONNECTION_ID,
      user_id: passkeyIdentity.user_id,
    }
  );
  console.log("Passkey identity linked to Auth0 primary user:", {
    primary: user.user_id,
    secondary: newUser.user_id,
  });
}

async function removePasskeyIdentity(user) {
  if (user.isPasskey) {
    throw new Error(
      "Removing a passkey identity from a passkey user is not supported"
    );
  }
  if (!user.passkeyIdentity) {
    throw new Error("User has no passkey identity to remove");
  }

  const { passkeyIdentity } = user;
  const secondaryUserId = `${passkeyIdentity.provider}|${passkeyIdentity.user_id}`;

  // unlink passkey user
  await auth0Management.users.unlink({
    id: user.user_id,
    provider: passkeyIdentity.provider,
    user_id: passkeyIdentity.user_id,
  });
  console.log("Passkey identity unlinked from Auth0 primary user:", {
    primary: user.user_id,
    secondary: secondaryUserId,
  });

  // delete passkey user
  await auth0Management.users.delete({
    id: secondaryUserId,
  });
  console.log("Auth0 passkey user removed:", { user_id: secondaryUserId });
}

module.exports = {
  getUser,
  addPasskeyIdentity,
  removePasskeyIdentity,
};
