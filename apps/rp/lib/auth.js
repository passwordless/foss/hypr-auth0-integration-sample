const jwt = require("jsonwebtoken");
const crypto = require("crypto");

const { ENFORCE_AAL_STEP_UP } = process.env;
const enforce_aal_step_up = ENFORCE_AAL_STEP_UP === "true";

function checkStepUpRequirement(req) {
  const { amr, authn_factors_available, authn_first_factor_performed } =
    req.oidc.idTokenClaims;

  const passkeyFactors = authn_factors_available
    .filter((f) => f.startsWith("passkey:"))
    .sort();

  if (passkeyFactors.length > 0) {
    const highestPasskeyFactor = passkeyFactors[passkeyFactors.length - 1];

    if (authn_first_factor_performed === "password") {
      return "passkey";
    } else if (
      enforce_aal_step_up &&
      authn_first_factor_performed.startsWith("passkey:") &&
      authn_first_factor_performed < highestPasskeyFactor
    ) {
      return "stronger_passkey";
    }
  } else if (
    passkeyFactors.length === 0 &&
    authn_factors_available.includes("mfa") &&
    !amr?.includes("mfa")
  ) {
    return "mfa";
  }

  return "none";
}

function requiresSession() {
  return (req, _res, next) => {
    if (!req.oidc.isAuthenticated()) {
      return next(
        new Error("Proxy endpoint requires an authenticated session")
      );
    }

    next();
  };
}

function requiresStepUpIfSession() {
  return (req, _res, next) => {
    if (req.oidc.isAuthenticated()) {
      const stepUpRequirement = checkStepUpRequirement(req);
      if (stepUpRequirement !== "none") {
        return next(
          new Error(`Current session requires step up to: ${stepUpRequirement}`)
        );
      }
    }
    next();
  };
}

function createPasskeyJwt(username, devices, thisDevice) {
  const user_id = crypto.createHash("md5").update(username).digest("hex");
  const token = jwt.sign(
    {
      username,
      user_id,
      devices,
      ...(thisDevice ? { this_device: thisDevice.device_id } : {}),
    },
    process.env.TOKEN_SECRET,
    {
      expiresIn: "5m",
    }
  );

  return token;
}

function validatePasskeyJwt(token) {
  jwt.verify(token, process.env.TOKEN_SECRET);
}

module.exports = {
  checkStepUpRequirement,
  requiresSession,
  requiresStepUpIfSession,
  createPasskeyJwt,
  validatePasskeyJwt,
};
