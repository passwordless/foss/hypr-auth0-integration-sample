const fetch = require("node-fetch");
const querystring = require("node:querystring");
const { DateTime } = require("luxon");

const { ErrorWithStatusCode } = require("../lib/error");

const { UAF_APP_NAME: uaf_app_name } = process.env;
const { UAF_ENABLED } = process.env;
const uaf_enabled = UAF_ENABLED === "true";

// helpers

const formatDate = (millis) =>
  DateTime.fromMillis(millis).toLocaleString(DateTime.DATETIME_SHORT);

// server access

/**
 * Helper function to perform an authenticated HTTP fetch to the HYPR server
 * @param {string} hyprPath The path on the HYPR server
 * @param {sting} method The HTTP method (eg. "GET") to use
 * @param {Object} options
 * @param {Object} [options.body] A JSON body to send (eg. if method = "POST")
 * @param {Object} [options.headers] Additional request HTTP headers to send
 * @param {boolean} [options.throwUserErrors=true] Throw an Error with 4** response from server
 * @param {boolean} [options.throwServerErrors=true] Throw an Error with 5** response from server
 * @param {boolean} [options.returnResponseBody=true] Attempt to parse and return a JSON server response
 * @param {boolean} [options.logActivity=true] console.log request/response activity
 * @returns {Promise} The fetch response or the JSON body, depending on options.returnResponseBody
 */
async function hyprServerFetch(hyprPath, method, options = {}) {
  const {
    body,
    headers,
    throwUserErrors = true,
    throwServerErrors = true,
    returnResponseBody = true,
    logActivity = true,
  } = options;

  const url = `${process.env.HYPR_BASE_URL}${hyprPath}`;
  const fetchOptions = {
    method,
    headers: {
      ...headers,
      Authorization: `Bearer ${process.env.HYPR_ACCESS_TOKEN}`,
      ...(body && { "Content-Type": "application/json" }),
    },
    ...(body && {
      body: JSON.stringify(body),
    }),
  };
  if (logActivity) {
    console.log("HYPR fetch::request", {
      hyprPath,
      method,
      ...(body && { body }),
      ...(headers && { headers }),
    });
  }

  const fetchResponse = await fetch(url, fetchOptions);

  const { status } = fetchResponse;
  let responseBody;
  if (returnResponseBody) {
    responseBody = await fetchResponse.json();
  }

  if (!fetchResponse.ok) {
    if (status < 500 && throwUserErrors) {
      throw new ErrorWithStatusCode(
        status,
        responseBody
          ? responseBody.title ?? JSON.stringify(responseBody)
          : fetchResponse.statusText
      );
    }
    if (status >= 500 && throwServerErrors) {
      throw new Error(
        `HYPR server fetch error: ${status}: ${
          responseBody ? JSON.stringify(responseBody) : fetchResponse.statusText
        }`
      );
    }
  }

  if (logActivity) {
    console.log("HYPR fetch::response", {
      status,
      ...(responseBody && { responseBody }),
    });
  }

  return returnResponseBody ? responseBody : fetchResponse;
}

/**
 * Helper function to perform an authenticated proxy request to the HYPR server
 * - request body and response body will be unaltered
 * - HTTP user agent will be passed through
 * @param {string} hyprPath The path on the HYPR server
 * @param {sting} method The HTTP method (eg. "GET") to use
 * @param {Request} req The HTTP request object of the proxy endpoint
 * @param {Response} res The HTTP response object of the proxy endpoint
 * @param {Object} options
 * @param {Object} [options.onCheckError] Called to check for an error in the response received from the HYPR server
 * @param {Object} [options.onPostResponse] Called after the response has been received from the HYPR server
 * @returns {Promise} (No resolved value)
 */
async function hyprServerProxy(hyprPath, method, req, res, options = {}) {
  const { onCheckError = () => false, onPostResponse } = options;

  const body = !["GET", "HEAD"].includes(method) ? req.body : undefined;
  console.log("HYPR proxy::request:", { method, hyprPath, body });

  const headers = { "User-Agent": req.headers["user-agent"] };

  const response = await hyprServerFetch(hyprPath, method, {
    body,
    headers,
    throwUserErrors: false,
    throwServerErrors: false,
    returnResponseBody: false,
    logActivity: false,
  });

  let status = response.status;
  let result = await response.json();
  console.log("HYPR proxy::response:", { status, result });

  if (response.ok && !onCheckError(result) && onPostResponse) {
    try {
      await onPostResponse(body, result);
    } catch (err) {
      console.log("HYPR proxy::onPostResponse error:", err);

      status = err.status && err.status < 500 ? 200 : 500;
      result = {
        status: "failed",
        errorMessage: err.message || err,
      };
    }
  }

  res.status(status).json(result);
}

// FIDO2 device management endpoints

async function getFido2Devices(username) {
  return (
    await hyprServerFetch(
      `/rp/api/versioned/fido2/user?${querystring.stringify({ username })}`,
      "GET"
    )
  )
    .map((device) => ({
      device_id: device.keyId,
      device_type: "fido2",
      display_name: device.authenticatorDisplayName,
      create_date: formatDate(device.createDate),
      is_synced: device.be && device.bs,
    }))
    .map((device) => ({
      ...device,
      aal: device.is_synced ? 2 : 3,
    }));
}

async function removeFido2Device(username, keyId) {
  await hyprServerFetch(
    `/rp/api/versioned/fido2/user?${querystring.stringify({
      username,
      keyId,
    })}`,
    "DELETE"
  );

  console.log("FIDO2 device removed");
}

// UAF device management endpoints

async function getUafDevices(username) {
  return (
    await hyprServerFetch(
      `/rp/api/oob/client/authentication/${process.env.HYPR_RP_APP_ID}/${username}/devices`,
      "GET"
    )
  ).map((device) => ({
    device_id: device.deviceId,
    device_type: "uaf",
    display_name: `${uaf_app_name}: ${device.friendlyName}`,
    create_date: formatDate(device.createDate),
    aal: 3,
  }));
}

async function removeUafDevice(username, deviceId) {
  await hyprServerFetch(
    `/rp/api/oob/client/device/delete/${process.env.HYPR_RP_APP_ID}/${username}/${deviceId}`,
    "POST",
    {
      body: {},
    }
  );

  console.log("FIDO2 device removed");
}

// aggregate

async function getAllDevices(username) {
  const [fido2Devices, uafDevices] = await Promise.all([
    getFido2Devices(username),
    uaf_enabled ? getUafDevices(username) : [],
  ]);

  return { fido2Devices, uafDevices };
}

module.exports = {
  hyprServerFetch,
  hyprServerProxy,
  getFido2Devices,
  removeFido2Device,
  getUafDevices,
  removeUafDevice,
  getAllDevices,
};
