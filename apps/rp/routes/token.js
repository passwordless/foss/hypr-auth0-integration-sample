const router = require("express").Router();
const { json } = require("body-parser");

const { validatePasskeyJwt } = require("../lib/auth");

router.post("/token/store", json(), async (req, res) => {
  const { token } = req.body;

  // only store token if its valid
  let isValid = false;
  try {
    validatePasskeyJwt(token);
    isValid = true;
  } catch (err) {}
  if (isValid) {
    req.appSession.autofillToken = token;
  }

  res.json({ isValid });
});

router.post("/token/recall", async (req, res) => {
  // extract and remove token
  const { autofillToken: token } = req.appSession;
  delete req.appSession.autofillToken;

  // only return token if its still valid
  if (token) {
    try {
      validatePasskeyJwt(token);
    } catch (err) {
      res.json({ token: null });
    }
  }

  res.json({ token });
});

module.exports = router;
