const router = require("express").Router();
const Handlebars = require("handlebars");

const source = `
body,
html {
  height: 100%;
  background-color: {{PAGE_BACKGROUND_COLOR}};
  background-image: url({{PAGE_BACKGROUND_IMAGE_URL}});
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
}

.login-container {
  position: relative;
  height: 100%;
  overflow-y: auto;
}

.login-box {
  top: 50%;
  transform: translateY(-50%);
  padding: 15px;
  background-color: #fff;
  box-shadow: 0px 5px 5px #ccc;
  border-radius: 5px;
  border-top: 1px solid #e9e9e9;
}

.login-header {
  text-align: center;
}

.login-header img {
  width: 200px;
}

#error-message {
  white-space: break-spaces;
  text-align: center;
}

.branded-button {
  background-color: {{PRIMARY_BUTTON_COLOR}};
  color: {{PRIMARY_LABEL_COLOR}};
}
`;

const template = Handlebars.compile(source);

router.get("/site.css", (_req, res) => {
  res.type("css").send(
    template({
      PAGE_BACKGROUND_COLOR: process.env.PAGE_BACKGROUND_COLOR,
      PAGE_BACKGROUND_IMAGE_URL: process.env.PAGE_BACKGROUND_IMAGE_URL,
      PRIMARY_BUTTON_COLOR: process.env.PRIMARY_BUTTON_COLOR,
      PRIMARY_LABEL_COLOR: process.env.PRIMARY_LABEL_COLOR,
    })
  );
});

module.exports = router;
