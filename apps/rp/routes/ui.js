const router = require("express").Router();
const { urlencoded, json } = require("body-parser");
const querystring = require("node:querystring");
const { requiresAuth } = require("express-openid-connect");

const {
  checkStepUpRequirement,
  requiresSession,
  requiresStepUpIfSession,
} = require("../lib/auth");
const {
  getUser,
  addPasskeyIdentity,
  removePasskeyIdentity,
} = require("../lib/auth0");
const {
  getAllDevices,
  removeFido2Device,
  removeUafDevice,
} = require("../lib/hypr");

const {
  ISSUER_BASE_URL,
  BASE_URL,
  UAF_ENABLED,
  UAF_APP_NAME: uaf_app_name,
  ENFORCE_AAL_STEP_UP,
} = process.env;
const uaf_enabled = UAF_ENABLED === "true";
const enforce_aal_step_up = ENFORCE_AAL_STEP_UP === "true";

// helpers

function leave(req, res) {
  const {
    state,
    returnTo = state ? `${ISSUER_BASE_URL}/continue?state=${state}` : "/",
  } = req.query;

  console.log("Logging out of the local session and redirecting to:", returnTo);

  res.oidc.logout({ returnTo });
}

async function syncAuth0WithHypr(email) {
  // get auth0 user
  const user = await getUser(email);
  if (!user) {
    throw new Error(`User no longer exists in IdP: ${email}`);
  }

  const { fido2Devices, uafDevices } = await getAllDevices(email);
  const devices = [...fido2Devices, ...uafDevices];

  let result = "no_change";
  if (devices.length > 0) {
    // hyper devices exist
    if (!user.isPasskey && !user.passkeyIdentity) {
      // no passkey identity exists, add one
      await addPasskeyIdentity(user);
      result = "passkey_identity_added";
    }
  } else {
    // no hypr devices exist
    if (user.passkeyIdentity) {
      // a passkey identity exists, remove it
      await removePasskeyIdentity(user);
      result = "passkey_identity_removed";
    }
  }

  console.log("Sync result:", result);
  return result;
}

// endpoints

router.get("/go_passwordless", requiresAuth(), (req, res) => {
  res.render("continue_or_cancel", {
    title: "Ready to go passwordless?",
    message: `Instead of using a password to sign in, now you can use a Passkey! 
No more forgetting your password or password resets.`,
    continue_button_text: "Let's do it!",
    cancel_button_text: "No thanks, maybe next time",
  });
});

router.post(
  "/go_passwordless",
  requiresAuth(),
  urlencoded({ extended: false }),
  (req, res) => {
    const { action } = req.body;

    switch (action) {
      case "continue":
        return res.redirect(`/passkeys?${querystring.stringify(req.query)}`);
      case "cancel":
        return leave(req, res);
      default:
        throw new Error(`Unsupported action: ${action}`);
    }
  }
);

router.get("/passkeys", requiresAuth(), async (req, res) => {
  const { email, authn_factors_available } = req.oidc.idTokenClaims;
  const { fido2Devices, uafDevices } = await getAllDevices(email);
  const devices = [...fido2Devices, ...uafDevices];

  // if user has strong and weak passkeys, get the strong ones
  let strongDeviceNames = [];
  if (enforce_aal_step_up) {
    const aals = Array.from(new Set(devices.map((d) => d.aal))).sort();
    if (aals.length > 1) {
      const strongestAal = aals[aals.length - 1];
      strongDeviceNames = devices
        .filter((d) => d.aal === strongestAal)
        .map((d) => d.display_name);
    }
  }

  // enforce the strongest factor possible
  const stepUpRequirement = checkStepUpRequirement(req);
  console.log("Step-up requirement:", stepUpRequirement);

  switch (stepUpRequirement) {
    case "passkey":
      // user needs to sign in with their passkey
      return res.render("continue_or_cancel", {
        title: "Passkey required",
        message:
          strongDeviceNames.length > 1
            ? "We need you to sign in with one of these strong passkeys first:"
            : "We need you to sign in with one of your passkeys first",
        content_list: strongDeviceNames,
      });

    case "stronger_passkey":
      // user needs to sign in with a stronger passkey
      return res.render("continue_or_cancel", {
        title: "Stronger passkey required",
        message:
          "We need you to sign in with one of these stronger passkey first:",
        content_list: strongDeviceNames,
      });

    case "mfa":
      // user is enrolled in MFA but hasn't used it in this session yet
      return res.render("continue_or_cancel", {
        title: "MFA required",
        message: "We need you to verify your identity with MFA first",
      });
  }

  // user has authenticated with the strongest factor possible

  const fido2_devices = fido2Devices.map((device) => ({
    ...device,
    // don't allow a passkey-only user to delete their last passkey
    can_delete:
      devices.length > 1 || authn_factors_available.includes("password"),
  }));

  const uaf_devices =
    uaf_enabled &&
    uafDevices.map((device) => ({
      ...device,
      // don't allow a passkey-only user to delete their last passkey
      can_delete:
        devices.length > 1 || authn_factors_available.includes("password"),
    }));

  res.render("passkey_manager", {
    title: "My Passkeys",
    username: email,
    BASE_URL,
    any_devices: devices.length > 0,
    fido2_devices,
    uaf_enabled,
    uaf_devices,
    uaf_app_name,
  });
});

router.post(
  "/passkeys",
  requiresAuth(),
  urlencoded({ extended: false }),
  async (req, res) => {
    const { action, device_id } = req.body;
    const { email } = req.oidc.idTokenClaims;

    switch (action) {
      case "continue":
        // perform step-up
        const returnTo =
          // we need to be able to pick up where we left off after the step-up flow
          `/passkeys?${querystring.stringify(req.query)}`;
        const stepUpRequirement = checkStepUpRequirement(req);

        const authorizationParams = {
          ...(stepUpRequirement.includes("passkey")
            ? {
                login_hint: email,
                prompt: "login",
              }
            : stepUpRequirement === "mfa" && {
                acr_values:
                  "http://schemas.openid.net/pape/policies/2007/06/multi-factor",
              }),
        };

        return res.oidc.login({
          returnTo,
          authorizationParams,
        });
      case "cancel":
      case "done":
        return leave(req, res);
      case "delete_fido2":
        await removeFido2Device(email, device_id);
        await syncAuth0WithHypr(email);

        // go back to passkey manager
        return res.redirect(`/passkeys?${querystring.stringify(req.query)}`);
      case "delete_uaf":
        if (uaf_enabled) {
          await removeUafDevice(email, device_id);
          await syncAuth0WithHypr(email);

          // go back to passkey manager
          return res.redirect(`/passkeys?${querystring.stringify(req.query)}`);
        }
      default:
        throw new Error(`Unsupported action: ${action}`);
    }
  }
);

router.post(
  "/sync",
  requiresSession(),
  requiresStepUpIfSession(),
  json(),
  async (req, res) => {
    const { email } = req.oidc.idTokenClaims;

    const result = await syncAuth0WithHypr(email);

    res.json({ result });
  }
);

module.exports = router;
