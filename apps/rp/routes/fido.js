const router = require("express").Router();
const { json } = require("body-parser");
const { randomBytes, createHash } = require("node:crypto");
const QRCode = require("qrcode");
const { requiresAuth } = require("express-openid-connect");

const { createPasskeyJwt, requiresStepUpIfSession } = require("../lib/auth");
const { getUser } = require("../lib/auth0");
const { BadRequestError } = require("../lib/error");
const {
  hyprServerProxy,
  hyprServerFetch,
  getAllDevices,
} = require("../lib/hypr");

const { UAF_ENABLED } = process.env;
const uaf_enabled = UAF_ENABLED === "true";

// helpers

function fido2ErrorCheck(result) {
  return result.status === "failed";
}

async function attachJwtForFido2(requestBody, resultBody) {
  const { username } = resultBody;
  const { fido2Devices, uafDevices } = await getAllDevices(username);
  const devices = [...fido2Devices, ...uafDevices];
  const thisDevice = fido2Devices.find((d) => d.device_id === requestBody.id);

  resultBody.token = createPasskeyJwt(username, devices, thisDevice);
}

function generatePin() {
  const data = randomBytes(1024);
  const hash = createHash("sha256");
  hash.update(data);
  return hash.digest("hex");
}

// FIDO2 registration and authentication endpoints
// - direct proxies of HYPR FIDO2 endpoints

router.post(
  "/fido2/attestation/options",
  json(),
  // prevent passkey creation if we don't have a strong session
  requiresStepUpIfSession(),
  (req, res) => {
    hyprServerProxy(
      "/rp/api/versioned/fido2/attestation/options",
      "POST",
      req,
      res,
      {
        onCheckError: fido2ErrorCheck,
        onPostResponse: async (_requestBody, resultBody) => {
          // enforce these restrictions for requests that occur outside of Passkey Manager
          if (!req.oidc.isAuthenticated()) {
            const { username } = req.body;

            // block registration when user already exists in HYPR
            if (resultBody.excludeCredentials.length > 0) {
              console.log(
                `/fido2/attestation/options: Registration blocked: User '${username}' already exists in FIDO server`
              );

              const error = new Error("Invalid sign up");
              error.status = 400;
              throw error;
            }

            // block registration when user already exists in Auth0
            if (await getUser(username)) {
              console.log(
                `/fido2/attestation/options: Registration blocked: User '${username}' already exists in IdP`
              );

              const error = new Error("Invalid sign up");
              error.status = 400;
              throw error;
            }
          }
        },
      }
    );
  }
);

router.post(
  "/fido2/attestation/result",
  json(),
  // prevent passkey creation if we don't have a strong session
  requiresStepUpIfSession(),
  (req, res) => {
    hyprServerProxy(
      "/rp/api/versioned/fido2/attestation/result",
      "POST",
      req,
      res,
      { onCheckError: fido2ErrorCheck, onPostResponse: attachJwtForFido2 }
    );
  }
);

router.post("/fido2/assertion/options", json(), (req, res) => {
  hyprServerProxy(
    "/rp/api/versioned/fido2/assertion/options",
    "POST",
    req,
    res,
    { onCheckError: fido2ErrorCheck }
  );
});

router.post("/fido2/assertion/result", json(), (req, res) => {
  hyprServerProxy(
    "/rp/api/versioned/fido2/assertion/result",
    "POST",
    req,
    res,
    { onCheckError: fido2ErrorCheck, onPostResponse: attachJwtForFido2 }
  );
});

// UAF registration and authentication endpoints
// - simplified abstractions of HYPR UAF endpoints

const UAF_MACHINE_ID = "PASSKEY_MANAGER";
const UAF_MACHINE_TYPE = "WEB";

const ensureUaf = () => (_req, _res, next) => {
  if (!uaf_enabled) {
    next(BadRequestError("UAF authentication not available"));
  }
  next();
};

router.post(
  "/fido_uaf/attestation/start",
  ensureUaf(),
  json(),
  // can only be called from Passkey Manager, which always requires auth
  requiresAuth(),
  // prevent UAF creation if we don't have a strong session
  requiresStepUpIfSession(),
  async (req, res) => {
    const { username } = req.body;
    if (!username) {
      throw BadRequestError("Missing: username");
    }

    // generate PIN
    const pin = generatePin();

    // create QR code
    const qrCodeCreateResult = await hyprServerFetch(
      "/rp/versioned/client/qr/create",
      "POST",
      {
        body: {
          pin,
          version: 4,
          rpAppId: process.env.HYPR_RP_APP_ID,
          rpUrl: `${process.env.HYPR_BASE_URL}/rp`,
        },
      }
    );
    const { qrCode } = qrCodeCreateResult;
    const qrCodeDataUrl = `data:image/gif;base64,${qrCode}`;

    // register client
    const registerClientResult = await hyprServerFetch(
      "/rp/api/client/reg",
      "POST",
      {
        body: {
          pin,
          client: {
            version: process.env.HYPR_API_VERSION,
            rpAppId: process.env.HYPR_RP_APP_ID,
            rpUrl: `${process.env.HYPR_BASE_URL}/rp`,
            machineId: UAF_MACHINE_ID,
            machineType: UAF_MACHINE_TYPE,
            machineName: process.env.UAF_MACHINE_NAME,
            machineUserName: username,
            sessionType: "CLIENT_INITIATED",
          },
        },
      }
    );
    const {
      response: {
        session: { sessionId },
      },
    } = registerClientResult;

    res.json({ qrCodeDataUrl, sessionId });
  }
);

router.get(
  "/fido_uaf/attestation/state/:sessionId",
  ensureUaf(),
  // can only be called from Passkey Manager, which always requires auth
  requiresAuth(),
  // prevent UAF creation if we don't have a strong session
  requiresStepUpIfSession(),
  async (req, res) => {
    const { sessionId } = req.params;
    if (!sessionId) {
      throw BadRequestError("Missing: sessionId");
    }

    const response = await hyprServerFetch("/rp/api/client/reg", "HEAD", {
      headers: { "HYPR-Session-ID": sessionId },
      throwUserErrors: false,
      returnResponseBody: false,
    });
    const { headers, status } = response;

    let state;
    if (status < 400) {
      state = headers.get("HYPR-Registration-State");
    } else {
      state = "UNKNOWN";
    }

    res.set("Cache-Control", ["no-store", "must-revalidate"]).json({ state });
  }
);

router.post(
  "/fido_uaf/assertion/start",
  ensureUaf(),
  json(),
  async (req, res) => {
    const { username, method } = req.body;
    if (!username) {
      throw BadRequestError("Missing: username");
    }
    if (!method) {
      throw BadRequestError("Missing: method");
    }

    let url;
    if (method === "qr_code") {
      url = "/rp/api/oob/qr/client/authentication/requests";
    } else if (method === "push") {
      url = "/rp/api/oob/client/authentication/requests";
    } else {
      throw BadRequestError(`Unsupported authentication method: ${method}`);
    }

    const body = {
      appId: process.env.HYPR_RP_APP_ID,
      machineId: UAF_MACHINE_ID,
      namedUser: username,
      machine: process.env.UAF_MACHINE_NAME,
      clientType: UAF_MACHINE_TYPE,
      sessionNonce: generatePin(),
      deviceNonce: generatePin(),
      serviceNonce: generatePin(),
      serviceHmac: generatePin(),
    };

    // perform assertion request
    const result = await hyprServerFetch(url, "POST", { body });
    const { landingPage, sessionId } = result;

    const qrCodeDataUrl =
      method === "qr_code" &&
      (await QRCode.toDataURL(landingPage, { type: "image/png", margin: 0 }));

    res.json({ qrCodeDataUrl, sessionId });
  }
);

router.get(
  "/fido_uaf/assertion/state/:sessionId",
  ensureUaf(),
  async (req, res) => {
    const { sessionId } = req.params;
    if (!sessionId) {
      throw BadRequestError("Missing: sessionId");
    }

    const result = await hyprServerFetch(
      `/rp/api/oob/client/authentication/requests/${sessionId}`,
      "GET"
    );
    const { state: states } = result;

    // get last state
    const lastState = states[states.length - 1];
    state = lastState.value;

    // build token
    let token;
    if (state === "COMPLETED") {
      const {
        namedUser,
        device: { deviceId },
      } = result;

      const { fido2Devices, uafDevices } = await getAllDevices(namedUser);
      const devices = [...fido2Devices, ...uafDevices];
      const thisDevice = uafDevices.find((d) => d.device_id === deviceId);

      token = await createPasskeyJwt(namedUser, devices, thisDevice);
    }

    res
      .set("Cache-Control", ["no-store", "must-revalidate"])
      .json({ state, token });
  }
);

// other FIDO-related endpoints

router.get("/fido/authentication_methods/:username", async (req, res) => {
  const { username } = req.params;

  const { fido2Devices, uafDevices } = await getAllDevices(username);

  const fido2 = fido2Devices.length > 0;
  const uaf = uafDevices.length > 0;
  res.json({ fido2, uaf });
});

module.exports = router;
