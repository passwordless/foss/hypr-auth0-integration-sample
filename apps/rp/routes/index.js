const router = require("express").Router();
const styles = require("./styles");
const ui = require("./ui");
const fido = require("./fido");
const token = require("./token");

router.get("/", (_req, res) => {
  res.send("ok");
});

router.get("/switch", (req, res) => {
  const { connection, login_hint, screen_hint } = req.query;
  if (!connection) {
    res.status(400).send("Connection parameter required");
  }
  if (screen_hint && screen_hint !== "signup") {
    res.status(400).send("Unsupported screen_hint");
  }

  res.oidc.login({
    authorizationParams: {
      connection,
      "ext-connection": connection,
      login_hint,
      screen_hint,
      prompt: "login",
    },
  });
});

router.use("/styles", styles);
router.use("/", ui);
router.use("/", fido);
router.use("/", token);

module.exports = router;
