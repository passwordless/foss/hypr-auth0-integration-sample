/**
 * Checks to see if the browser supports passkey autofill
 * @returns {Promise} True if the browser supports passkey autofill
 */
async function isPasskeyAutofillSupported() {
  if (
    typeof window.PublicKeyCredential !== "undefined" &&
    typeof window.PublicKeyCredential.isConditionalMediationAvailable ===
      "function"
  ) {
    const supported =
      await PublicKeyCredential.isConditionalMediationAvailable();

    if (supported) {
      console.log("Passkey autofill supported:", supported);
      return true;
    }
  }

  return false;
}

/**
 * Perform a complete passkey sign-in flow
 * @param {Object} options
 * @param {string} options.rpBaseUrl - The base URL of the FIDO RP server
 * @param {string} [options.username] - The username of the existing user
 * @param {string} [options.userVerification=preferred] - The user verification type
 * @param {string} [options.authenticatorAttachment=platform] - The authenticator attachment type
 * @param {boolean} [options.attemptAutofill=false] - True if attempting passkey autofill
 * @param {AbortSignal} options.abortSignal - An AbortSignal object used to abort the authentication (eg. if using autofill)
 * @returns {Promise} A sign-in result that contains a username and a JWT token
 */
async function authenticatePasskey(options) {
  // only continue if passkeys are supported on this browser
  await assurePasskeySupport();

  const {
    rpBaseUrl,
    username,
    userVerification,
    authenticatorAttachment,
    attemptAutofill,
    abortSignal,
  } = options;

  // perform options call
  const assertionOptions = HYPRFido2Client.createAssertionOptions(
    username,
    userVerification,
    authenticatorAttachment
  );
  const optionsResponse = await fidoFetch(
    `${options.rpBaseUrl}/fido2/assertion/options`,
    assertionOptions,
    "Assertion options request"
  );

  // only continue if there's a registered passkey or we're attempting passkey autofill
  if (!!optionsResponse.allowCredentials?.length || attemptAutofill) {
    // attempt to authenticate with a passkey
    const credentialResult = await usePasskey(
      optionsResponse,
      attemptAutofill,
      abortSignal
    );

    // perform result call
    const assertionResult = await fidoFetch(
      `${rpBaseUrl}/fido2/assertion/result`,
      credentialResult,
      "Assertion result request"
    );

    return assertionResult;
  }
}

/**
 * Performs a complete passkey sign-up flow
 * @param {Object} options
 * @param {string} options.rpBaseUrl - The base URL of the FIDO RP server
 * @param {string} options.username - The username of the new user
 * @param {string} options.displayName - The display name of the new user
 * @param {string} [options.authenticatorAttachment=platform] - The authenticator attachment type
 * @param {string} [options.userVerification=required] - The user verification type
 * @param {string} [options.isResidentKeyRequired=false] - Whether or not to use a resident key
 * @param {string} [options.attestationType=direct] - The attestation type to use for registration
 * @returns {Promise} A sign-up result that contains the attestation result from the FIDO server
 */
async function registerPasskey(options) {
  // only continue if passkeys are supported on this browser
  await assurePasskeySupport();

  const { username, displayName, rpBaseUrl } = options;
  const attestationOptions = {
    username,
    displayName,
  };

  const optionsResponse = await fidoFetch(
    `${rpBaseUrl}/fido2/attestation/options`,
    attestationOptions,
    "Attestation options request"
  );

  // attempt to create a passkey
  const credentialResult = await createPasskey(optionsResponse);

  // perform result call
  const attestationResult = await fidoFetch(
    `${rpBaseUrl}/fido2/attestation/result`,
    credentialResult,
    "Attestation result request"
  );

  return attestationResult;
}

/**
 * Performs a complete UAF sign-in flow using a QR code
 * @param {Object} options
 * @param {string} options.rpBaseUrl - The base URL of the FIDO RP server
 * @param {string} options.username - The username of the existing user
 * @param {Function} options.displayQrCode - called when a new QR code needs to be displayed
 * @param {Function} options.displayPendingMessage - called when a registration has begun in the UAF app
 * @param {Function} options.displayTimeoutMessage - called when registration has taken too long
 * @param {Function} [options.pollingSleepDuration=3000] - the duration (in milliseconds) to sleep between registration status polling
 * @param {AbortSignal} options.abortSignal - An AbortSignal object used to abort the registration status polling
 * @returns {Promise} A boolean indicating if a new device was actually registered (true) or if the process was cancelled (false)
 */
async function authenticateUaf(options) {
  const {
    rpBaseUrl,
    username,
    displayQrCode,
    displayPendingMessage,
    displayTimeoutMessage,
    pollingSleepDuration = 3000,
    abortSignal,
  } = options;

  // obtain QR code and session ID
  const startResult = await serverFetch(
    `${rpBaseUrl}/fido_uaf/assertion/start`,
    "POST",
    { username, method: "qr_code" }
  );
  const { qrCodeDataUrl, sessionId } = startResult;

  displayQrCode(qrCodeDataUrl);

  // begin polling for completed state
  const COMPLETED_STATES = ["COMPLETE", "TIMEOUT", "UNKNOWN"];
  let state;
  while (!abortSignal.aborted && !COMPLETED_STATES.includes(state)) {
    const stateResult = await serverFetch(
      `${rpBaseUrl}/fido_uaf/assertion/state/${sessionId}`,
      "GET"
    );
    let { state, token } = stateResult;

    switch (state) {
      case "INITIATED":
      case "INITIATED_RESPONSE":
      case "COMPLETED_INIT":
        displayPendingMessage();
        break;

      case "COMPLETED":
        return { username, token };

      case "TIMEOUT":
      case "CANCELED":
        displayTimeoutMessage();
        return;
    }

    // sleep between polling attempts
    await sleep(pollingSleepDuration);
  }
}

/**
 * Performs a complete UAF sign-up flow
 * @param {Object} options
 * @param {string} options.rpBaseUrl - The base URL of the FIDO RP server
 * @param {string} options.username - The username of the new user
 * @param {Function} options.displayQrCode - called when a new QR code needs to be displayed
 * @param {Function} options.displayPendingMessage - called when a registration has begun in the UAF app
 * @param {Function} options.displayTimeoutMessage - called when registration has taken too long
 * @param {Function} [options.pollingSleepDuration=3000] - the duration (in milliseconds) to sleep between registration status polling
 * @param {AbortSignal} options.abortSignal - An AbortSignal object used to abort the registration status polling
 * @returns {Promise} A boolean indicating if a new device was actually registered (true) or if the process was cancelled (false)
 */
async function registerUaf(options) {
  const {
    rpBaseUrl,
    username,
    displayQrCode,
    displayPendingMessage,
    displayTimeoutMessage,
    pollingSleepDuration = 3000,
    abortSignal,
  } = options;

  // obtain QR code and session ID
  const startResult = await serverFetch(
    `${rpBaseUrl}/fido_uaf/attestation/start`,
    "POST",
    { username }
  );
  const { qrCodeDataUrl, sessionId } = startResult;

  displayQrCode(qrCodeDataUrl);

  // begin polling for completed state
  const COMPLETED_STATES = ["COMPLETE", "TIMEOUT", "UNKNOWN"];
  let state;
  while (!abortSignal.aborted && !COMPLETED_STATES.includes(state)) {
    const stateResult = await serverFetch(
      `${rpBaseUrl}/fido_uaf/attestation/state/${sessionId}`,
      "GET"
    );
    let { state } = stateResult;

    switch (state) {
      case "PENDING_REG":
      case "FIDO_GET":
        displayPendingMessage();
        break;

      case "COMPLETE":
        return { username };

      case "TIMEOUT":
      case "CANCELLED":
      case "UNKNOWN":
        displayTimeoutMessage();
        return;
    }

    // sleep between polling attempts
    await sleep(pollingSleepDuration);
  }
}
