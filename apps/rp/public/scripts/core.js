// core helper function

async function assurePasskeySupport() {
  return new Promise((resolve, reject) => {
    HYPRFido2Client.isFido2Available((error, result) => {
      console.log("Browser supports passkeys:", result);

      if (error) {
        return reject(error);
      }
      resolve(result);
    }, false);
  });
}

async function serverFetch(url, method, jsonRequest) {
  const fetchOptions = {
    method,
    cache: "no-cache",
    credentials: "include",
    ...(jsonRequest && {
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(jsonRequest),
    }),
  };

  const fetchResponse = await fetch(url, fetchOptions);

  // ensure there were no HTTP errors with the call
  if (!fetchResponse.ok) {
    console.error(
      "FIDO server fetch: HTTP Error:",
      JSON.stringify(
        {
          url: fetchResponse.url,
          status: fetchResponse.status,
          statusText: fetchResponse.statusText,
          body: await fetchResponse.text(),
        },
        null,
        2
      )
    );
    throw new Error("Error communicating with the FIDO server");
  }
  const jsonResponse = await fetchResponse.json();
  console.log(
    "FIDO server fetch: Response:",
    JSON.stringify(
      {
        url: fetchResponse.url,
        status: fetchResponse.status,
        response: jsonResponse,
      },
      null,
      2
    )
  );

  return jsonResponse;
}

async function fidoFetch(url, jsonRequest) {
  const jsonResponse = await serverFetch(url, "POST", jsonRequest);

  // check for FIDO error
  if (jsonResponse.status != "ok") {
    throw new Error(jsonResponse.errorMessage);
  }

  return jsonResponse;
}

async function usePasskey(
  serverAssertionOptionsResponse,
  attemptAutofill,
  abortSignal
) {
  // convert credential IDs from base64 url encoded strings to byte arrays
  const options = {
    ...serverAssertionOptionsResponse,
    allowCredentials: serverAssertionOptionsResponse.allowCredentials?.map(
      (cred) => ({
        ...cred,
        id: base64ToArrayBuffer(fromBase64URL(cred.id)),
      })
    ),
    ...(attemptAutofill && { mediation: "conditional" }),
    abortSignal,
  };

  return new Promise((resolve, reject) => {
    HYPRFido2Client.useFido2Credential(options, (error, result) => {
      if (error) {
        return reject(error);
      }
      console.log("Use passkey result:", JSON.stringify(result, null, 2));

      resolve(result);
    });
  });
}

async function createPasskey(serverAttestationOptionsResponse) {
  // NOTE: unlike usePasskey, no need to transform anything
  const options = {
    ...serverAttestationOptionsResponse,
  };

  return new Promise((resolve, reject) => {
    HYPRFido2Client.createFido2Credential(
      options,
      (error, result) => {
        if (error) {
          return reject(error);
        }
        console.log("Create passkey result:", JSON.stringify(result, null, 2));

        resolve(result);
      },
      // removeExcludeCredentials = false
      false
    );
  });
}

async function sleep(duration) {
  return new Promise((resolve) => setTimeout(resolve, duration));
}
