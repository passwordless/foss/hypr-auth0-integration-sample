/**
 * Checks to see if a user has an existing device (FIDO2)
 * @param {Object} options
 * @param {string} options.rpBaseUrl - The base URL of the FIDO RP server
 * @param {string} options.username - The username of existing user
 * @returns {Promise} True if any devices exist for the user
 */
async function getFidoAuthenticationMethods(options) {
  const { rpBaseUrl, username } = options;

  const devicesResult = await serverFetch(
    `${rpBaseUrl}/fido/authentication_methods/${username}`,
    "GET"
  );

  return devicesResult;
}

/**
 * Forces the RP server to sync the passkey registrations from the HYPR server to the Auth0 server
 * @param {Object} options
 * @param {string} options.rpBaseUrl - The base URL of the FIDO RP server
 * @returns {Promise}
 */
async function syncDevices(options) {
  const { rpBaseUrl } = options;

  await serverFetch(`${rpBaseUrl}/sync`, "POST");
}

/**
 * Stores a JWT passkey security token in the RP; can be extracted with #recallToken
 * @param {Object} options
 * @param {string} options.rpBaseUrl - The base URL of the FIDO RP server
 * @param {string} token - the token to store
 */
async function storeToken(options, token) {
  const { rpBaseUrl } = options;

  return serverFetch(`${rpBaseUrl}/token/store`, "POST", { token });
}

/**
 * Restores a JWT passkey security token that was stored in the RP with #storeToken
 * @param {Object} options
 * @param {string} options.rpBaseUrl - The base URL of the FIDO RP server
 * @returns {Promise} A string containing the token, if one was available
 */
async function recallToken(options) {
  const { rpBaseUrl } = options;

  const recallResponse = await serverFetch(`${rpBaseUrl}/token/recall`, "POST");

  return recallResponse.token;
}
