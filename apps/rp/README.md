# Passkey Manager App

The **Passkey Manager** web app primary role is to provide a UI for users to manage their passkeys. This UI can be triggered in the middle of an Auth0 login flow (via an [Action redirect](https://auth0.com/docs/customize/actions/flows-and-triggers/login-flow/redirect-with-actions)) or when a user navigates directly to it. The Passkey Manager also proxies FIDO requests from the user's browser to the HYPR server when WebAuthn authentication or registration flows occur. This is necessary because all FIDO requests to a HYPR server must be authenticated with an access token. Therefore, calls directly from a web browser are not possible. This web app is also called the [Relying Party](https://www.hypr.com/security-encyclopedia/relying-party-rp) (RP) since this is the role it plays in relation to the FIDO server (HYPR).

The Passkey Manager doesn't require a database, since its backend effectively is the HYPR server as well as the Auth0 server. One of its jobs is to keep the Auth0 server "in sync" with the state of users' passkeys on the HYPR server.

## Custom domain

The RP app needs to be configured to run (both locally or remote) with a custom domain that shares a parent domain with the [Auth0 tenant](../../infra/auth0/).  
Example:

- RP app: `passkeys.`**`example.com`**
- Auth0 tenant: `login.`**`example.com`**

## Local setup

1. Create a `.env` file with the the following settings:

   > See [Configuration](../../CONFIG.md) for descriptions of each config item.

   - `CLIENT_ID`
   - `CLIENT_SECRET`
   - `ISSUER_BASE_URL`
   - `COOKIE_SECRET`
   - `NODE_ENV`
     - Set to `development` for local development
   - `PORT`:
     - Local: `4433`
     - Hosted: `80`
   - `BASE_URL`
     - Local: `https://RP_APP_DOMAIN:4433/`
     - Hosted: `https://RP_APP_DOMAIN/`
   - `AUTH0_DOMAIN`
   - `TOKEN_SECRET`
   - `PASSKEY_DB_CONNECTION`
   - `PASSKEY_DB_CONNECTION_ID`
   - `HYPR_BASE_URL`
   - `HYPR_ACCESS_TOKEN`
   - `HYPR_RP_APP_ID`
   - `HYPR_API_VERSION`
   - `LOGO_URL`
   - `PAGE_BACKGROUND_IMAGE_URL`
   - `PRIMARY_BUTTON_COLOR`
   - `PAGE_BACKGROUND_COLOR`
   - `PRIMARY_LABEL_COLOR`
   - `UAF_ENABLED`
   - `UAF_APP_NAME`
   - `UAF_MACHINE_NAME`
   - `ENFORCE_AAL_STEP_UP`

   where `RP_APP_DOMAIN` is the RP app's [custom domain](#custom-domain)

1. Add the following line to the `/etc/hosts` file:

   ```text
   127.0.0.1  RP_APP_DOMAIN
   ```

   where `RP_APP_DOMAIN` is the RP app's [custom domain](#custom-domain)

1. Install the self-signed dev certificate

   ```shell
   cd ./cert
   bash ../../../scripts/create-dev-cert.sh RP_APP_DOMAIN
   bash ../../../scripts/install-dev-cert.sh
   ```

   where `RP_APP_DOMAIN` is the RP app's [custom domain](#custom-domain)

1. Install dependencies

   ```shell
   npm install
   ```

## Run locally

Start the server

```shell
npm start
```
