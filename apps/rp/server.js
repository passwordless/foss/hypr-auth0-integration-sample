const dotenv = require("dotenv");
dotenv.load();
require("express-async-errors");
const express = require("express");
const cors = require("cors");
const http = require("http");
const https = require("https");
const fs = require("fs");
const logger = require("morgan");
const path = require("path");
const { auth } = require("express-openid-connect");
const { engine } = require("express-handlebars");

const package = require("./package.json");
const routes = require("./routes");

const app = express();

app.use(logger("dev"));
app.use(express.static(path.join(__dirname, "public")));
app.use(express.json());
app.use(
  cors({
    origin: [process.env.ISSUER_BASE_URL],
    credentials: true,
  })
);

app.set("view engine", "handlebars");
app.engine(
  "handlebars",
  engine({
    defaultLayout: "main",
  })
);

// data available in all views
app.use((_req, res, next) => {
  res.locals.HYPR_BASE_URL = process.env.HYPR_BASE_URL;
  res.locals.LOGO_URL = process.env.LOGO_URL;

  next();
});

// OIDC
app.use(
  auth({
    authRequired: false,
    auth0Logout: false,
    secret: process.env.COOKIE_SECRET,

    getLoginState: (req, options) => {
      // support restart of login flow without losing original returnTo
      const { originalReturnTo } = req.appSession;
      if (originalReturnTo) {
        options.returnTo = originalReturnTo;
      } else if (options.returnTo) {
        req.appSession.originalReturnTo = options.returnTo;
      }

      return options;
    },

    afterCallback: (req, _res, session, _decodedState) => {
      // clear originalReturnTo when flow completes
      delete req.appSession.originalReturnTo;
      return session;
    },
  })
);

const port = process.env.PORT || 3000;

// routes

app.use(routes);

// catch 404 and forward to error handler
app.use(function (_req, _res, next) {
  const err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handlers
app.use(function (err, _req, res, _next) {
  const status = err.status || 500;
  if (status >= 500) {
    console.error("ERROR:", err);
  }

  res.status(status);
  res.json({
    message: err.message,
    error: process.env.NODE_ENV !== "production" ? err : {},
  });
});

const server =
  process.env.NODE_ENV === "production"
    ? http.createServer(app)
    : https.createServer(
        {
          key: fs.readFileSync(path.resolve("./cert/dev.key")),
          cert: fs.readFileSync(path.resolve("./cert/dev.crt")),
        },
        app
      );

server.listen(port, () => {
  console.log(
    `${package.description} (v${package.version}), listening on: ${process.env.BASE_URL}`
  );
});
