# Web App

The **Web App** provides a simple Node.js web application to use in the sample as the app that's integrating with Auth0. It's essentially the [Express OpenID Connect Webapp Sample](https://github.com/auth0-samples/auth0-express-webapp-sample/tree/master/01-Login) provided by Auth0 with just a couple minor changes.

## App domain

The web app must use a domain other than `localhost`. When run locally, it uses a self-signed SSL certificate associated with this domain.  
Example:  
`web.example.com`

## Local setup

1. Create a `.env` file with the the following settings:

   > See [Configuration](../../CONFIG.md) for descriptions of each config item.

   - `CLIENT_ID`
   - `ISSUER_BASE_URL`
   - `COOKIE_SECRET`
   - `NODE_ENV`
     - Set to `development` for local development
   - `PORT`:
     - Local: `4444`
     - Hosted: `80`
   - `BASE_URL`
     - Local: `https://WEB_APP_DOMAIN:4444/`
     - Hosted: `https://WEB_APP_DOMAIN/`
   - `RP_APP_BASE_URL`

   where `WEB_APP_DOMAIN` is the web app's [domain](#app-domain)

1. Add the following line to the `/etc/hosts` file:

   ```text
   127.0.0.1  WEB_APP_DOMAIN
   ```

   where `WEB_APP_DOMAIN` is the web app's [domain](#app-domain)

1. Install the self-signed dev certificate

   ```shell
   cd ./cert
   bash ../../../scripts/create-dev-cert.sh WEB_APP_DOMAIN
   bash ../../../scripts/install-dev-cert.sh
   ```

   where `WEB_APP_DOMAIN` is the web app's [domain](#app-domain)

1. Install dependencies

   ```shell
   npm install
   ```

## Run locally

1. Start the server

   ```shell
   npm start
   ```

2. Access the URL displayed in the log output in the terminal
