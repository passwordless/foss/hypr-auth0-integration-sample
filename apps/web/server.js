const dotenv = require("dotenv");
const express = require("express");
const http = require("http");
const https = require("https");
const fs = require("fs");
const logger = require("morgan");
const path = require("path");
const router = require("./routes/index");
const { auth } = require("express-openid-connect");

const package = require("./package.json");

dotenv.load();

const app = express();

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(logger("dev"));
app.use(express.static(path.join(__dirname, "public")));
app.use(express.json());

// OIDC
app.use(
  auth({
    authRequired: false,
    auth0Logout: true,
    secret: process.env.COOKIE_SECRET,

    getLoginState: (req, options) => {
      // support restart of login flow without losing original returnTo
      const { originalReturnTo } = req.appSession;
      if (originalReturnTo) {
        options.returnTo = originalReturnTo;
      } else if (options.returnTo) {
        req.appSession.originalReturnTo = options.returnTo;
      }

      return options;
    },

    afterCallback: (req, _res, session, _decodedState) => {
      // clear originalReturnTo when flow completes
      delete req.appSession.originalReturnTo;
      return session;
    },
  })
);

const port = process.env.PORT || 3000;

// Middleware to make the `user` object available for all views
app.use(function (req, res, next) {
  res.locals.user = req.oidc.user;
  next();
});

app.use("/", router);

// Catch 404 and forward to error handler
app.use(function (_req, _res, next) {
  const err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// Error handlers
app.use(function (err, _req, res, _next) {
  res.status(err.status || 500);
  res.render("error", {
    message: err.message,
    error: process.env.NODE_ENV !== "production" ? err : {},
  });
});

const server =
  process.env.NODE_ENV === "production"
    ? http.createServer(app)
    : https.createServer(
        {
          key: fs.readFileSync(path.resolve("./cert/dev.key")),
          cert: fs.readFileSync(path.resolve("./cert/dev.crt")),
        },
        app
      );

server.listen(port, () => {
  console.log(
    `${package.description} (v${package.version}), listening on: ${process.env.BASE_URL}`
  );
});
