var router = require("express").Router();
const { requiresAuth } = require("express-openid-connect");

router.get("/", (req, res) => {
  res.render("index", {
    title: "Auth0 Webapp sample Nodejs",
    isAuthenticated: req.oidc.isAuthenticated(),
  });
});

router.get("/profile", requiresAuth(), (req, res) => {
  res.render("profile", {
    userProfile: JSON.stringify(req.oidc.user, null, 2),
    title: "Profile page",
    passkey_manager_url: `${
      process.env.RP_APP_BASE_URL
    }/passkeys?returnTo=${encodeURIComponent(
      process.env.BASE_URL + "/profile"
    )}`,
  });
});

router.get("/force_mfa", (req, res) => {
  return res.oidc.login({
    returnTo: "/profile",
    authorizationParams: {
      acr_values:
        "http://schemas.openid.net/pape/policies/2007/06/multi-factor",
    },
  });
});

router.get("/switch", (req, res) => {
  const { connection, login_hint, screen_hint } = req.query;
  if (!connection) {
    res.status(400).send("Connection parameter required");
  }
  if (screen_hint && screen_hint !== "signup") {
    res.status(400).send("Unsupported screen_hint");
  }

  res.oidc.login({
    authorizationParams: {
      connection,
      "ext-connection": connection,
      login_hint,
      screen_hint,
      prompt: "login",
    },
  });
});

module.exports = router;
