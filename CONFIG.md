# Sample Configuration

For the sample to work, configuration must be set up in a handful of places, which are called out in respective `README.md` files. However, many config items are the same across these areas. This page provides a central place where all config values are defined.

## OIDC configuration in web applications

Config items used by web applications in the solution for OIDC integration with the [Auth0 tenant](./infra/auth0/).

### `CLIENT_ID`

The `client_id` of the Auth0 application.

### `CLIENT_SECRET`

The `client_secret` of the Auth0 application.

> Only needed if the app needs to present client credentials to Auth0.

### `ISSUER_BASE_URL`

The base URL of the OIDC issuer, which includes the custom domain of the Auth0 tenant.

Eg. `https://login.example.com`

## Other web application configuration

Other config items used by web applications in the solution.

### `COOKIE_SECRET`

Used to encrypt/decrypt the local session cookie.

### `BASE_URL`

The base URL of the web application.

> No ending slash

### `NODE_ENV`

The Node.js environment of the web app.

### `PORT`

The HTTP port the web app will listen on.

## Auth0 tenant

Config items for other entities within the [Auth0 tenant](./infra/auth0/).

### `AUTH0_DOMAIN`

The tenant domain (vs. custom domain) of the Auth0 tenant.

Eg. `example.us.auth0.com`

### `PRIMARY_WEB_APP_NAME`

The friendly name of the [Web App](./apps/web) app.

### `PRIMARY_WEB_APP_BASE_URL`

The `BASE_URL` of the [Web App](./apps/web) app.

> No ending slash

### `RP_APP_NAME`

The friendly name of the [Passkey Manager](./apps/rp) app.

### `RP_APP_BASE_URL`

The `BASE_URL` of the [Passkey Manager](./apps/rp) app.

> No ending slash

### `PASSWORD_DB_CONNECTION`

The name of the Auth0 database connection used for password authentication.

### `PASSKEY_DB_CONNECTION`

The name of the Auth0 database connection used for passkey authentication.

### `PASSKEY_DB_CONNECTION_ID`

The ID of the Auth0 database connection used for passkey authentication.

### `ACTIONS_M2M_CLIENT_ID` / `ACTIONS_M2M_CLIENT_SECRET`

The client credentials used by Auth0 Actions when obtaining an access token (via an OAuth 2.0 client credential grant call) to use with the Auth0 Management API.

### `USE_CLASSIC_UL`

Set to `true` to present the passwordless experience using [Classic Universal Login](https://auth0.com/docs/authenticate/login/auth0-universal-login/classic-experience); otherwise `false` to present it using [New Universal Login](https://auth0.com/docs/authenticate/login/auth0-universal-login/new-experience).

### `PASSKEY_AUTOFILL_ENABLED`

Set to `true` to enable FIDO2 passkey autofill, aka [WebAuthn Conditional UI](https://passkeys.dev/docs/use-cases/bootstrapping/). This will autofill the username field of the login page with discoverable passkeys.

## Authentication behavior

### `UAF_ENABLED`

Set to `true` if UAF devices (app-bound passkeys) will be supported in the login and Passkey Manager UX or just FIDO2 passkeys.

### `ENFORCE_AAL_STEP_UP`

Set to `true` to force step-up to stronger FIDO passkeys if they have been registered.

## HYPR tenant

### `HYPR_BASE_URL`

The base URL of the HYPR FIDO server.

### `HYPR_RP_APP_ID`

The camelCase identifier of the RP app taken from the URL when viewing the app in the Control Center.

### `HYPR_API_VERSION`

The version of the HYPR service being used with certain endpoints.

### `HYPR_ACCESS_TOKEN`

The access token that can be used to make HTTP calls to the HYPR tenant.

## Security interop

### `TOKEN_SECRET`

The secret used to sign and then verify the JWT that is passed from the [Passkey Manager](./apps/rp) app (after a successful authentication with HYPR) to the passkey custom database connection in the Auth0 tenant.

## Branding

### `LOGO_URL`

The logo URL.

Eg. `https://demo.hypr.com/sample/images/hblogonotext.png`

### `PAGE_BACKGROUND_IMAGE_URL`

The URL of the background image used with all login and passkey manager prompts.

Eg. `https://live.staticflickr.com/7448/11993656886_9203556376_b.jpg`

### `PRIMARY_BUTTON_COLOR`

The primary button color.

Eg. `#57abbf`

### `PRIMARY_LABEL_COLOR`

The primary button label text color.

Eg. `#ffffff`

### `PAGE_BACKGROUND_COLOR`

The page background color (if `PAGE_BACKGROUND_IMAGE_URL` isn't used).

Eg. `#ffffff`

### `TENANT_FRIENDLY_NAME`

The friendly name of the Auth0 tenant.

Eg. `HYPR Highlands Bank Demo`

### `TENANT_NEW_UL_TITLE`

The title used on New Universal Login pages.

Eg. `Highlands Bank`

### `UAF_APP_NAME`

The name of the mobile app used for UAF authentication.

Eg. `HYPR App`

### `UAF_MACHINE_NAME`

The name of the website that appears in the UAF app.

Eg. `Highlands Bank`
